#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <poll.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include <wayland-server.h>

#define CLIENTS_CAP 64

static void set_nonblock(int fd) {
	int flags = fcntl(fd, F_GETFL, 0);
	if (flags < 0) {
		abort();
	}
	int ret = fcntl(fd, F_SETFL, flags | O_NONBLOCK);
	if (ret < 0) {
		abort();
	}
}

static void bind_global(struct wl_client *client, void *data, uint32_t id, uint32_t version) {
	fprintf(stderr, "Client bound to global\n");
}

int main(int argc, char *argv[]) {
	struct wl_display *display = wl_display_create2();

	wl_global_create(display, &wl_output_interface, 1, NULL, bind_global);

	int server_fd = socket(PF_UNIX, SOCK_STREAM, 0);
	if (server_fd < 0) {
		abort();
	}
	set_nonblock(server_fd);

	const char display_name[] = "wayland-custom";
	struct sockaddr_un addr = {
		.sun_family = AF_UNIX,
	};
	snprintf(addr.sun_path, sizeof(addr.sun_path), "%s/%s", getenv("XDG_RUNTIME_DIR"), display_name);

	unlink(addr.sun_path);

	int ret = bind(server_fd, (struct sockaddr *) &addr, sizeof(addr));
	if (ret < 0) {
		abort();
	}
	ret = listen(server_fd, 64);
	if (ret < 0) {
		abort();
	}

	fprintf(stderr, "Listening on WAYLAND_DISPLAY=%s\n", display_name);

	size_t clients_len = 0;
	struct wl_client *clients[CLIENTS_CAP] = {0};

	struct pollfd pollfds[1 + CLIENTS_CAP] = {0};
	pollfds[0] = (struct pollfd) { .fd = server_fd, .events = POLLIN };
	struct pollfd *client_pollfds = &pollfds[1];

	while (1) {
		ret = poll(pollfds, 1 + clients_len, -1);
		if (ret < 0) {
			abort();
		}

		if (pollfds[0].revents & POLLIN) {
			if (clients_len >= CLIENTS_CAP) {
				abort();
			}

			int client_fd = accept(server_fd, NULL, NULL);
			if (client_fd < 0) {
				abort();
			}
			set_nonblock(client_fd);

			client_pollfds[clients_len] = (struct pollfd) {
				.fd = client_fd,
				.events = POLLIN,
			};

			struct wl_client *client = wl_client_create(display, client_fd);
			clients[clients_len] = client;

			fprintf(stderr, "New client #%zu\n", clients_len);
			clients_len++;
		}

		for (size_t i = 0; i < clients_len; i++) {
			if (client_pollfds[i].revents & (POLLHUP | POLLERR)) {
				fprintf(stderr, "Client #%zu disconnected\n", i);
				wl_client_destroy(clients[i]);
				client_pollfds[i] = (struct pollfd) { .fd = -1 };
				continue;
			}

			if (client_pollfds[i].revents & POLLOUT) {
				errno = 0;
				wl_client_flush(clients[i]);
				if (errno == 0) {
					client_pollfds[i].events &= ~POLLOUT;
				}
			}

			if (client_pollfds[i].revents & POLLIN) {
				wl_client_dispatch(clients[i]);

				errno = 0;
				wl_client_flush(clients[i]);
				if (errno == EAGAIN) {
					client_pollfds[i].events |= POLLOUT;
				}
			}
		}
	}
}
